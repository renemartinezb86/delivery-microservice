/**
 * View Models used by Spring MVC REST controllers.
 */
package setit.delivering.microservice.web.rest.vm;
